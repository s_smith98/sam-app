import json
import numpy as np

# lambda_handler method signature
def lambda_handler(event,context):    
    jackpot= np.random.randint(0,9,10)
    one_million = np.random.randint(0,9,10)
    minor_winners = [str(np.random.randint(0,9,10)),str(np.random.randint(0,9,10)),str(np.random.randint(0,9,10)),str(np.random.randint(0,9,10))] 
    return {
        "statusCode": 200,
        "body": json.dumps({
            "Jackpot Number": str(jackpot),
            "1M winning numbers": str(one_million),
            "Other winning numbers": minor_winners
        }),
    }
